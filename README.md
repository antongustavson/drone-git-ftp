# Drone CI plugin wrapper for git-ftp

The plugin uses `push` and `auto-init` to update files on the server.

## Usage

You have to set the password for your FTP server in the `FTP_PASSWORD` secret.

Use `args` to pass arguments directly to git ftp. ([All args can be seen here](https://github.com/git-ftp/git-ftp/blob/master/man/git-ftp.1.md))

.drone.yml:

```yaml
pipeline:
  deploy:
    image: mkanton/drone-git-ftp:latest
    username: ftpuser
    hostname: ftp://hostename/path/to/destination
    secrets: [ ftp_password ]
    args: --verbose
```